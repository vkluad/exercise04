#include <stdio.h>
#include <string.h>
#include "game.h"

long bet = 0;
unsigned short game_point = 0;
long lost_bet = 0;

void argument_checker(int argc, char *argv[], long *count_players)
{
	char *ptr;
	for (int i = 1; i < argc; i++) {
		if ((strcmp(argv[i], "-c") == 0) ||
		    (strcmp(argv[i], "--count-player") == 0)) {
			i++;
			*count_players = strtol(argv[i], &ptr, 10);
			continue;
		}
		if ((strcmp(argv[i], "-b") == 0) ||
		    (strcmp(argv[i], "--bet") == 0)) {
			i++;
			bet = strtol(argv[i], &ptr, 10);
			continue;
		}
	}
}

int main(int argc, char *argv[])
{
	struct list_node *head = NULL;
	struct list_node *cursor = NULL;
	struct players *player = NULL;
	int win_multiplier;
	long count_players = 0;
	char temp[10] = "";
	char *temp_ptr = NULL;

	argument_checker(argc, argv, &count_players);

	while ((count_players < 2) || (count_players > 15)) {
		printf("How players will be playing: ");
		scanf("%s", temp); // safe scanf
		count_players = strtol(temp, &temp_ptr, 10);
		while (getchar() != '\n') // safe scanf
			;
	}

	for (int i = 0; i < count_players; i++) {
		char player_name[PLAYER_NAME_SIZE] = "";
		printf("Enter name of the player%d: ", i);
		fgets(player_name, PLAYER_NAME_SIZE, stdin);
		strtok(player_name, "\n");
		player = players_create(player_name, i);
		cursor = list_push(&player->node, cursor);
		if (head == NULL)
			head = cursor;
	}

	while (bet < 1) {
		bet = 0;
		printf("Enter the bet: ");
		scanf("%s", temp); // safe scanf
		bet = strtol(temp, &temp_ptr, 10);
		while (getchar() != '\n') // safe scanf
			;
	}

	head = draw(head);
	game_point = one_dice_throw();

	/***************** Game circle ********************/
	while (is_losing_players(head)) {
		cursor = head;
		each_of_list(cursor)
		{
			player = container_of(cursor, struct players, node);
			if (!player->is_win) {
				win_multiplier = move(player);
				player->price = win_multiplier * bet;
				lost_bet -= player->price;
			}
		}
	}
	/********************************************************/

	/*********************** Game output *************************/
	cursor = head;

	printf("\nGame point: %d\n", game_point);
	printf("Rate: %ld\n\n", bet);
	printf("-----------------------------------------------------------------------------------------------------------------\n");
	printf("ID\t| Players\t| Dice1\t| Dice2\t| Dice3\t| Score\t| Is_win | Jackpot\t| Price\t\t| Count move\t|\n");
	each_of_list(cursor)
	{
		if (!player->is_win)
			player->price = lost_bet;
		player = container_of(cursor, struct players, node);
		printf("%d\t| %s\t\t| %d\t| %d\t| %d\t| %d\t| %s\t | %s \t| %ld\t\t| %d\t\t|\n",
		       player->id, player->player_name,
		       player->last_pos_dice[0], player->last_pos_dice[1],
		       player->last_pos_dice[2], player->score,
		       player->is_win ? "true" : "false",
		       player->jackpot ? "true" : "false", player->price,
		       player->count_move);
	}
	printf("-----------------------------------------------------------------------------------------------------------------\n\n");
	/********************** Game output end **********************/

	list_destroy(head); // destroy all malloc memory
	free(player);
	return 0;
}
