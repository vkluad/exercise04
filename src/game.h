#ifndef EXERCISE04_GAME_H
#define EXERCISE04_GAME_H

#include <time.h>
#include "list.h"

extern long bet;
extern long lost_bet;
extern unsigned short game_point;

int one_dice_throw(void)
{
	srand((unsigned)clock());
	return 1 + rand() % 6;
}

_Bool jackpot_check(struct players *player)
{
	if ((player->last_pos_dice[0] == player->last_pos_dice[1]) &&
	    (player->last_pos_dice[1] == player->last_pos_dice[2]) &&
	    (player->last_pos_dice[2] == game_point))
		return 1;
	return 0;
}

int move(struct players *player)
{
	int temp = 0;
	player->count_move++;
	for (int i = 0; i < 3; i++) {
		player->last_pos_dice[i] = one_dice_throw();
		if (player->last_pos_dice[i] == game_point)
			temp += 1;
	}
	if ((player->last_pos_dice[0] == player->last_pos_dice[1]) &&
	    (player->last_pos_dice[1] == player->last_pos_dice[2]) &&
	    (player->last_pos_dice[2] == (temp / 3)))
		temp = 5;

	player->score += temp;

	if ((player->jackpot = jackpot_check(player))) {
		player->score = 15;
		player->is_win = 1;
		return 2;
	}
	if (player->score >= 15) {
		player->is_win = 1;
		return 1;
	}
	return 0;
}

void first_move(struct players *player)
{
	for (int i = 0; i < 3; i++) {
		player->last_pos_dice[i] = one_dice_throw();
		player->score += player->last_pos_dice[i];
	}
}

struct list_node *draw(struct list_node *head)
{
	int max_score = 0;
	struct players *p = NULL;
	struct list_node *pos = head;
	struct list_node *new_head = NULL;

	each_of_list(pos)
	{
		p = container_of(pos, struct players, node);
		first_move(p);
		if (p->score > max_score)
			max_score = p->score;
	}

	pos = head;

	each_of_list(pos)
	{
		p = container_of(pos, struct players, node);
		if ((max_score == p->score) && (max_score != 0)) {
			max_score = 0;
			if (pos != head) {
				pos = list_del_entry(pos);
				pos = list_insert(pos, NULL, head);
				new_head = pos;
			} else {
				new_head = head;
			}
		}
		p->score = 0;
	}
	return new_head;
}

int is_losing_players(struct list_node *head)
{
	int count = 0;
	struct players *p = NULL;
	struct list_node *pos = head;
	each_of_list(pos)
	{
		p = container_of(pos, struct players, node);
		if (!p->is_win) {
			count++;
		}
	}
	if (count > 1)
		return 1;
	return 0;
}

#endif //EXERCISE04_GAME_H
