#ifndef EXERCISE04_LIST_H
#define EXERCISE04_LIST_H
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#define PLAYER_NAME_SIZE 8
#define each_of_list(cursor) for (; (cursor) != NULL; (cursor) = (cursor)->next)

struct list_node {
	struct list_node *next;
	struct list_node *prev;
};

struct players {
	struct list_node node;
	unsigned int id;
	char player_name[PLAYER_NAME_SIZE + 1];
	int score;
	int last_pos_dice[3];
	long price;
	unsigned int count_move;
	_Bool jackpot;
	_Bool is_win;
};

#define container_of(ptr, type, member)                                        \
	((type *)((char *)(1 ? (ptr) : &((type *)0)->member) -                 \
		  offsetof(type, member)))

struct list_node *list_init(struct list_node *head)
{
	head->next = NULL;
	head->prev = NULL;
	return head;
}

struct list_node *list_push(struct list_node *new, struct list_node *current)
{
	if (current == NULL)
		return list_init(new);
	new->prev = current;
	new->next = NULL;
	current->next = new;
	return new;
}

struct list_node *list_insert(struct list_node *new, struct list_node *prev,
			      struct list_node *next)
{
	new->prev = prev;
	new->next = next;
	if (next != NULL)
		next->prev = new;
	if (prev != NULL)
		prev->next = new;
	return new;
}

//struct list_node *list_del_after(struct list_node *current)
//{
//	struct list_node *deleted = current->next;
//	if (current->next->next != NULL)
//		current->next->next->prev = current;
//	if (current->next != NULL)
//		current->next = current->next->next;
//	return deleted;
//}

struct list_node *list_del_entry(struct list_node *current)
{
	struct list_node *deleted = current;
	if (current->prev != NULL)
		current->prev->next = current->next;
	if (current->next != NULL)
		current->next->prev = current->prev;
	deleted->next = NULL;
	deleted->prev = NULL;
	return deleted;
}

//struct list_node *list_del_before(struct list_node *current)
//{
//	struct list_node *deleted = current->prev;
//	if (current->prev != NULL)
//		current->prev = current->prev->prev;
//	if (current->prev->prev != NULL)
//		current->prev->prev->next = current;
//	return deleted;
//}

/**
 * Initialization all elements in struct player for liquidation  memory leak
 **/

struct players *players_create(char player_name[PLAYER_NAME_SIZE], int id)
{
	struct players *p = malloc(sizeof(struct players));
	p->id = id;
	strcpy(p->player_name, player_name);
	p->score = 0;
	p->count_move = 0;
	p->price = 0;
	p->is_win = 0;
	p->jackpot = 0;
	for (int i = 0; i < 3; i++) {
		p->last_pos_dice[i] = 0;
	}
	return p;
}
/****************************************************************************/

void list_destroy(struct list_node *head)
{
	struct list_node *cursor = NULL;
	struct players *player = NULL;
	while (head != NULL) {
		cursor = head;
		head = head->prev;
		cursor = list_del_entry(cursor);
		player = container_of(cursor, struct players, node);
		free(player);
	}
}

#endif //EXERCISE04_LIST_H
