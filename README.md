# Exercise04
### Simple game of dice

 You can use option to set count of players and set a bet\
  -b or --bet : for set a bet \
> For example `./game -b 250` or `./game --bet 250`
 
 -c or --count-player : for set count of player but not more then 15\
> For example `./game -c 5` or `./game --count-player`
 
 Or can use this options both:
> `./game -c 5 -b 250` or `./game --count-player 5 --bet 250`
 
 For make this program use `make all` and you can find this program in `bin` directory
