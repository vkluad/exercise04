.PHONY: all clean debug
all: bin
	gcc src/main.c -o bin/game

debug:
	gcc -g src/main.c -o bin/game

clean:
	rm -rf bin

bin:
	mkdir bin
